package inteviewcamp.subarrayproblems

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.SUB_ARRAY_PROBLEMS
import kotlin.math.max

// Given an array of integers that can be both +ve and -ve, find the contiguous subarray with lagest sum
// there is no point having only positive number array in this proble.
class KadaneAlgorithm : BaseInterviewCamp() {

    override var concept: String = SUB_ARRAY_PROBLEMS
    override fun initInformation(): Information = Information(
        questionCount = 4.1,
        problemUrl = "https://leetcode.com/problems/maximum-subarray/",
        solutionUrl = "https://www.youtube.com/watch?v=0yQ1lVbXLIM",
        source = INTERVIEW_CAMP
    )

    override fun solution() {

        val array: Array<Int> = arrayOf(1, 2, -1, 2, -3, 2, -5)
        val result = bruteForceSolutions(array)
        println("result  brute force $result")
        val kadanResult =  kadaneSolution(array)
        println("kadane result $kadanResult")

    }

    fun bruteForceSolutions(array: Array<Int>): Int {
        var maxSum = 0;
        for (i in array.indices) {
            var sumOfLoop = 0
            for (j in i until array.size) {
                sumOfLoop += array[j];
                if (sumOfLoop > maxSum) {
                    maxSum = sumOfLoop
                }
            }
        }
        return maxSum
    }


    fun kadaneSolution(array: Array<Int>): Int {
        var maxSum =  Integer.MIN_VALUE
        var currentSum = Integer.MIN_VALUE;
        for (i in array.indices){
            var loopMax = 0;
            currentSum += array[i]
            if(currentSum > array[i]){ // comparing current sum and array element is the key
                loopMax =  currentSum
            }else{
                loopMax = array[i]
            }
            if(maxSum< loopMax){
                maxSum = loopMax
            }
            currentSum = loopMax
        }
        return maxSum
    }

}

fun main() {
    KadaneAlgorithm().solution()
}