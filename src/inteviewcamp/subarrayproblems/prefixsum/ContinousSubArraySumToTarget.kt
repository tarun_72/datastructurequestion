package inteviewcamp.subarrayproblems.prefixsum

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.SUB_ARRAY_PROBLEMS

class ContinousSubArraySumToTarget  : BaseInterviewCamp() {

    override var concept: String = SUB_ARRAY_PROBLEMS
    override fun initInformation(): Information = Information(
        questionCount = 4.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {

        val array: Array<Int> = arrayOf(2,4,-2,1,-3,5,-3)
        // 2,6,4,5,2,7,4
        val result = preSumSubArraySolution(array,5)
        result.forEach {
            it?.let {
                println(it )
            }
        }
        //println("pre Sum Sub Array Solution $result")
    }

    fun preSumSubArraySolution(array: Array<Int>,target:Int): Array<Int?> {
        var sum = 0
       var map: HashMap<Int, Int> = HashMap()
       for (i in array.indices){
           sum += array[i]
           if(sum == 5){
               return arrayOf(0,i)
           }
           val checkInt =  sum - target
           if(map.containsKey(checkInt)){
               var value = map[sum]?.plus(1)
               return arrayOf(value,i)
           }
           map.put(sum,i)
       }
        return arrayOf()
    }
}


fun main() {
    ContinousSubArraySumToZero().solution()
}