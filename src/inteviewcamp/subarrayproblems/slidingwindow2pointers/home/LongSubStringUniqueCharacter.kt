package inteviewcamp.subarrayproblems.slidingwindow2pointers.home

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.SUB_ARRAY_PROBLEMS

//(Level: Medium) Given a String, find the longest substring with unique characters.
class LongSubStringUniqueCharacter : BaseInterviewCamp() {

    override var concept: String = SUB_ARRAY_PROBLEMS
    override fun initInformation(): Information = Information(
        questionCount = 4.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        var input = "whatwhywhere"
       var result =  longestUniqueSubstring(input)
        result.forEach {
            print(it)
        }

    }

    fun bruteForce(input:String) : Int{
        val arrayResult =  input.toCharArray()
        for(i in arrayResult.indices){
            for(j in i until arrayResult.size){
                // all sub string will be here
            // #TODO complete brute force

            }
        }


        return 0;

    }

    fun longestUniqueSubstring(input:String):String{
    val arrayResult =  input.toCharArray()

        // #TODO Longest Unique Sub String

        return ""
    }
}