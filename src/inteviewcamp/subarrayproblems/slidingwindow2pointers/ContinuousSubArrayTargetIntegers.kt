package inteviewcamp.subarrayproblems.slidingwindow2pointers

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.SUB_ARRAY_PROBLEMS

//Given an array of positive integers, find the contiguous subarray that sums to a given number X.
class ContinuousSubArrayTargetIntegers : BaseInterviewCamp() {

    override var concept: String = SUB_ARRAY_PROBLEMS
    override fun initInformation(): Information = Information(
        questionCount = 4.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(1, 2, 3, 5, 2)
        var result = slidingWindowTwoPointer(array, 8)
        result.forEach {
            println("=> $it and value => ${array[it]}")
        }
    }

    private fun slidingWindowTwoPointer(array: Array<Int>, target: Int): Array<Int> {
        var startPointerOfWindow = 0
        var endPointerOfWindow = 0
        var sumOfWindow = array[startPointerOfWindow]
        while (startPointerOfWindow < array.size) {
            // At the start of Loop
            // if number is greater than Target
            // start and end will be pointing same number
            if (startPointerOfWindow > endPointerOfWindow) { //start inched forward, bring end back to start
                endPointerOfWindow = startPointerOfWindow
                sumOfWindow = array[startPointerOfWindow]
            }

            if (sumOfWindow < target) {
                if (endPointerOfWindow >= (array.size - 1)) {
                    break
                }
                endPointerOfWindow++ // we increase endPointer first than add it
                sumOfWindow += array[endPointerOfWindow]
            } else if (sumOfWindow > target) {
                sumOfWindow -= array[startPointerOfWindow]
                startPointerOfWindow++ //  we increase startPointer after decreasing
            } else {
                return arrayOf(startPointerOfWindow, endPointerOfWindow)
            }
        }
        return arrayOf()
    }
}

fun main() {
    ContinuousSubArrayTargetIntegers().solution()
}