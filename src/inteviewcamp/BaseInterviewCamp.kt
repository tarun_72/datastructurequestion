package inteviewcamp

import DataStructureQuestion

const val  TRAVERSE_FROM_BOTH_END =  "Traverse from Both Ends"
const val  PARTITION_ARRAY =  "Partition Array"
const val  SUB_ARRAY_PROBLEMS =  "Subarray Problems"
const val  BINARY_SEARCH =  "Binary Search Problems"
const val  RECURSION_MEMOIZATION =  "RECURSION MEMOIZATION"




abstract  class BaseInterviewCamp : DataStructureQuestion() {
    abstract var concept:String
}
