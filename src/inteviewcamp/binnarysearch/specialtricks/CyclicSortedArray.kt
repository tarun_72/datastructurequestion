package inteviewcamp.binnarysearch.specialtricks

import INTERVIEW_CAMP
import inteviewcamp.BINARY_SEARCH
import inteviewcamp.BaseInterviewCamp


//1. (Level: Easy) Given a sorted array A that has been rotated in a cycle,
// find the smallest element of the array in O(log(n)) time. For example,
//#TODO home work remaining
class CyclicSortedArray : BaseInterviewCamp() {
    override var concept: String = BINARY_SEARCH
    override fun initInformation(): Information = Information(
        questionCount = 5.5,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array:Array<Int> = arrayOf(4,5,1,2,3)

        println("${cyclic(array)}")
    }


    fun cyclic(sortedArray: Array<Int>): Int {

        var low = 0
        var high: Int = sortedArray.size - 1
        var right: Int = sortedArray.get(sortedArray.size - 1)
        while (low <= high) {
            var mid = (low + (high - low)) / 2
            if(sortedArray[mid] <= right && (mid == 0 || sortedArray[mid - 1] > sortedArray[mid])){
                return mid
            }
            else if (sortedArray[mid] > right) {// bigger half
                low += 1
            } else if (sortedArray[mid] < low) {
                high -= 1
            }
        }
        return 0
    }

}

fun main() {
    CyclicSortedArray().solution()
}