package inteviewcamp.binnarysearch

import INTERVIEW_CAMP
import inteviewcamp.BINARY_SEARCH
import inteviewcamp.BaseInterviewCamp

class BinnarSearchDuplicate : BaseInterviewCamp() {
    override var concept: String = BINARY_SEARCH
    override fun initInformation(): Information = Information(
        questionCount = 5.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array:Array<Int> = arrayOf(1,1,2,3,4,5,6,7,8,9,9,9,9,10,11,12,13,14,15,16,17)
       val result =  binnarySearch(sortedArray = array,
                        target = 9)
        println("item is place on $result")
    }


    fun binnarySearch(sortedArray: Array<Int>, target: Int): Int {
        var start = 0
        var end = sortedArray.size - 1
        var mid = start + (end - start) / 2
        while (start <= end) {
            if (sortedArray[mid] > target
                || (sortedArray[mid] == target && mid > 0 && sortedArray[mid-1] ==target )) {
                end = mid - 1
            } else if (sortedArray[mid] < target) {
                start = mid + 1
            } else {
                return mid
            }
             mid = start + (end - start) / 2
        }

        return -1;
    }
}

fun main() {
    BinnarSearchDuplicate().solution()
}