package inteviewcamp.binnarysearch

import INTERVIEW_CAMP
import inteviewcamp.BINARY_SEARCH
import inteviewcamp.BaseInterviewCamp

// Record and Move on
class SearchClosest : BaseInterviewCamp() {
    override var concept: String = BINARY_SEARCH
    override fun initInformation(): Information = Information(
        questionCount = 5.1,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(1, 2, 4, 5, 7, 8, 9)
        val result = binnarySearch(
            sortedArray = array,
            target = 17
        )
        println("=== $result")
    }


    fun binnarySearch(sortedArray: Array<Int>, target: Int): Int {
        // binnary Search with record and move on Stratergies
        var start: Int = 0
        var end = sortedArray.size - 1
        var result = Integer.MAX_VALUE

        while (start <= end) {
            var mid = start + (end - start) / 2 // escape from overflow error
            result = recordMidEvent(sortedArray, target, mid, result)
            if (sortedArray[mid] > target) {
                end = mid - 1
            } else if (sortedArray[mid] < target) {
                start = mid + 1
            } else {
                return mid
            }
        }
        return result
    }

    private fun recordMidEvent(sortedArray: Array<Int>, target: Int, mid: Int, result: Int): Int {
        if (result == Integer.MAX_VALUE) {
            return mid
        }
        // check who is better
        if (difference(sortedArray[mid], target) < difference(result, target)) {
            return mid
        } else {
            return result
        }
    }

    private fun difference(item: Int, target: Int): Int {
        return Math.abs(item - target)
    }
}

fun main() {
    SearchClosest().solution()
}