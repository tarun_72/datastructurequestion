package inteviewcamp.recursion

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.RECURSION_MEMOIZATION
/**
 * Power Function: Implement a function to calculate xn.
 * Both x and n can be positive/negative and overflow doesn't happen. Try doing it in O(log(n)) time.
 * */
// Incorrect solutions
class IntroHome :  BaseInterviewCamp() {
    override var concept: String = RECURSION_MEMOIZATION
    override fun initInformation(): Information = Information(
        questionCount = 6.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        println("${positivePower(2,-2)}")
    }

    fun power(x:Int,power:Int):Float{
            var result =  positivePower(Math.abs(x),Math.abs(power))
        if(power < 0){
            result = 1 / result
        }
        if(x<0 && power % 2 != 0){
            result *= -1;
        }
        return result
    }


    // to find out positive power
    fun positivePower(x:Int,power:Int):Float{
        if(power == 0){
            return 1.0F
        }
        if(power == 1){
            return x.toFloat()
        }
        val halfPower =  positivePower(x,power/2)
        return if(Math.abs(halfPower) % 2 == 0.0F){
            halfPower * halfPower
        }else{
            x * halfPower * halfPower
        }

    }

}

fun main() {
    IntroHome().solution()
}