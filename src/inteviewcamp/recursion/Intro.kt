package inteviewcamp.recursion

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.RECURSION_MEMOIZATION

class Intro : BaseInterviewCamp() {
    override var concept: String = RECURSION_MEMOIZATION
    override fun initInformation(): Information = Information(
        questionCount = 6.1,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        println(fibonacciSeries(8))
        var map: HashMap<Int, Int> = HashMap()
        println(fibonacciSeriesWithMemonization(8, map))
    }

    /**
     *Time Complexity: O(2n) exponential
     *Space Complexity: O(n)
     * */
    fun fibonacciSeries(number: Int): Int {
        if (number == 1 || number == 2) {
            return 1
        }
        return fibonacciSeries(number - 1) + fibonacciSeries(number - 2)
    }

    /**
     *Time Complexity: O(n)
     *Space Complexity: O(n)
     * */
    fun fibonacciSeriesWithMemonization(number: Int, map: HashMap<Int, Int>): Int? {
        if (number == 1 || number == 2) {
            return 1
        }
        if (map.containsKey(number)) {
            return map.get(number)
        }
        var result = fibonacciSeries(number - 1) + fibonacciSeries(number - 2)
        map[number] = result
        return result
    }
}

fun main() {
    Intro().solution()
}