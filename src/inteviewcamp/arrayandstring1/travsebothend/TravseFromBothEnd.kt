package inteviewcamp.arrayandstring1.travsebothend

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.TRAVERSE_FROM_BOTH_END

class TravseFromBothEnd : BaseInterviewCamp() {

    override var concept: String = TRAVERSE_FROM_BOTH_END
    override fun initInformation(): Information = Information(
        questionCount = 2.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val arrof: Array<Int> = arrayOf(1, 2, 3, 4, 5, 6)
        var target: Int = 9
        var arr = findArray(arrof, target)
        arr.forEach {
            println(it)
        }
    }

    fun findArray(arrof: Array<Int>, target: Int): Array<Int> {
        var left: Int = 0
        var right: Int = arrof.size - 1
        while (left <= right) {
            val value = arrof[left] + arrof[right]
            if (value > target) {
                right--
            } else if (value < target) {
                left++
            } else {
                return arrayOf(arrof[left], arrof[right])
            }
        }
        return arrayOf(0, 0)
    }
}

fun main() {
    TravseFromBothEnd().solution()
}