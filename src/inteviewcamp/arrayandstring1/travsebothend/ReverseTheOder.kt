package inteviewcamp.arrayandstring1.travsebothend

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.TRAVERSE_FROM_BOTH_END

class ReverseTheOder() : BaseInterviewCamp() {

    override var concept: String = TRAVERSE_FROM_BOTH_END
    override fun initInformation(): Information = Information(
        questionCount = 2.1,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )
    override fun solution() {
        val arrof: Array<Int> = arrayOf (1,2,3,4,5,6)
        val arr = reverseOrder(arrof)
        arr.forEach {
            println(it)
        }
    }

    fun reverseOrder(arrof: Array<Int>) : Array<Int>{
        var left:Int = 0
        var right:Int =  arrof.size-1

        while (left<=right){
            swap(left,right,arrof)
            left++
            right--
        }
        return arrof
    }

    private fun swap(left: Int, right: Int, arrof: Array<Int>) {
        var temp =  arrof[left]
        arrof[left] = arrof[right]
        arrof[right] = temp
    }
}

fun main() {
    ReverseTheOder().solution()
}