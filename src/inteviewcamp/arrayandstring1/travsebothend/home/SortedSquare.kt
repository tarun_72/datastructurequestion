package inteviewcamp.arrayandstring1.travsebothend.home

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.TRAVERSE_FROM_BOTH_END
import kotlin.math.abs
// 2.4 remaining
class SortedSquare : BaseInterviewCamp() {

    override var concept: String = TRAVERSE_FROM_BOTH_END
    override fun initInformation(): Information = Information(
        questionCount = 2.3,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(-4, -2, -1, 0, 3, 5)
       val result =  square(array)
        result.forEach {
            println(it)
        }
    }

    fun square(array: Array<Int>) : Array<Int>{
        var result = Array<Int>(array.size) { 0 }

        var left: Int = 0
        var right: Int = array.size - 1
        var counter = 0
        while (left <= right) {
            var square = 0
            if (abs(array[left]) > abs(array[right])) {
                square = array[left] * array[left]
                left++
            } else {
                square = array[right] * array[right]
                right--
            }
            result[counter] = square
            counter++
        }
        return result
    }

}

fun main() {
    SortedSquare().solution()
}