package inteviewcamp.arrayandstring1.partitionarray

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.PARTITION_ARRAY

// if u want same order array
// we need to try second version of the problem which is adding zero at the end
class ZeroAtTheBegining : BaseInterviewCamp() {

    override var concept: String = PARTITION_ARRAY
    override fun initInformation(): Information = Information(
        questionCount = 3.1,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(4, 2, 0, 1, 0, 3, 0)
        val result = moveZerosAtTheStart(array)
        result.forEach {
            println(it)
        }
    }

    fun moveZerosAtTheStart(array: Array<Int>): Array<Int> {
        var lowBounder = 0
        for (runner in array.indices) {
            if (array[runner] == 0) {
                swap(lowBounder, runner, array)
                lowBounder++
            }
        }
        return array
    }

    private fun swap(left: Int, right: Int, arrof: Array<Int>) {
        var temp = arrof[left]
        arrof[left] = arrof[right]
        arrof[right] = temp
    }

}


fun main() {
    ZeroAtTheBegining().solution()
}








