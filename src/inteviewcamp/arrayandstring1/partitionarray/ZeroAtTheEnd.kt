package inteviewcamp.arrayandstring1.partitionarray

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.PARTITION_ARRAY

class ZeroAtTheEnd : BaseInterviewCamp() {

    override var concept: String = PARTITION_ARRAY
    override fun initInformation(): Information = Information(
        questionCount = 3.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(4, 2, 0, 1, 0, 3, 0)
        val result = moveZerosAtTheEnd(array)
        result.forEach {
            println(it)
        }
    }

    private fun moveZerosAtTheEnd(array: Array<Int>): Array<Int> {
        var higherBound = array.size - 1
        for (runner in (array.size-1) downTo  0){
            if (array[runner] == 0) {
                swap(runner, higherBound, array)
                higherBound--
            }
        }
        return array
    }

    private fun swap(left: Int, right: Int, arrof: Array<Int>) {
        var temp = arrof[left]
        arrof[left] = arrof[right]
        arrof[right] = temp
    }
}

fun main() {
    ZeroAtTheEnd().solution()
}
