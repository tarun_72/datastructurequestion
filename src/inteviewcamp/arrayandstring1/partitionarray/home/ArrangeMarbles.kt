package inteviewcamp.arrayandstring1.partitionarray.home

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.PARTITION_ARRAY

class ArrangeMarbles : BaseInterviewCamp() {

    override var concept: String = PARTITION_ARRAY
    override fun initInformation(): Information = Information(
        questionCount = 3.4,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(1, 0, 1, 2, 1, 0, 1, 2)
        val result = arrangeMarbleByColor(array)
        result.forEach {
            0
            println(it)
        }
    }

    private fun arrangeMarbleByColor(array: Array<Int>): Array<Int> {
        var runner = 0
        var lowBound = 0
        var highBound = array.size - 1
        while (runner <= highBound) {
            if (array[runner] == 1) {
                runner++
            } else if (array[runner] > 1) {
                swap(runner, highBound, array)
                highBound--
            } else {
                swap(runner, lowBound, array)
                lowBound++
                runner++
            }
        }
        return array
    }

    private fun swap(left: Int, right: Int, arrof: Array<Int>) {
        var temp = arrof[left]
        arrof[left] = arrof[right]
        arrof[right] = temp
    }
}

fun main() {
    ArrangeMarbles().solution()
}