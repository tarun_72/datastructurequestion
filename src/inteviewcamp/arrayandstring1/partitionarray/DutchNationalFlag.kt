package inteviewcamp.arrayandstring1.partitionarray

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp
import inteviewcamp.PARTITION_ARRAY

class DutchNationalFlag : BaseInterviewCamp() {

    override var concept: String = PARTITION_ARRAY
    override fun initInformation(): Information = Information(
        questionCount = 3.3,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val array: Array<Int> = arrayOf(5, 2, 4, 4, 6, 4, 4, 3)
        val result = isDutchFlag(array, 4)
        result.forEach {
            println(it)
        }
    }

    fun isDutchFlag(array: Array<Int>, pivot: Int): Array<Int> {
        // two pivot element
        var lowBoundary = 0
        var highBoundary = array.size - 1
        var runner = 0
        while (runner <= highBoundary)
            if (array[runner] < pivot) {
                swap(runner, lowBoundary, array)
                lowBoundary += 1
                runner++
            } else if (array[runner] > pivot) {
                swap(runner, highBoundary, array)
                highBoundary -= 1

            } else {
                runner++
            }
        return array
    }

    private fun swap(left: Int, right: Int, arrof: Array<Int>) {
        var temp = arrof[left]
        arrof[left] = arrof[right]
        arrof[right] = temp
    }

}

fun main() {
    DutchNationalFlag().solution()
}