package inteviewcamp.arrayandstring1

import INTERVIEW_CAMP
import inteviewcamp.BaseInterviewCamp

class DoubleEvenNumbers() : BaseInterviewCamp() {
    override var concept: String = "Reverse Array"
    override fun initInformation(): Information = Information(
        questionCount = 1.1,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        val arrof: Array<Int> = arrayOf (1,2,5,6,8)
        val arr = doubleEvens(arrof)
        arr.forEach {
            println(it)
        }
    }


    fun doubleEvens(arrof: Array<Int>): Array<Int?> {
        val totalSizeRequire = getSize(arrof)
        var timeArray: Array<Int?> = arrayOfNulls<Int>(totalSizeRequire)
        var previousLast = arrof.size - 1
        var newLast = totalSizeRequire - 1

        for (i in (arrof.size - 1) downTo 0) {
            if (arrof[i] % 2 == 0) {
                timeArray[newLast] = arrof[previousLast]
                newLast--
            }
            timeArray[newLast] = arrof[previousLast]
            previousLast--
            newLast--
        }
        return timeArray
    }

    private fun getSize(arrof: Array<Int>): Int {
        var totalEvenCount = 0
        for (i in arrof.indices) {
            if (arrof[i] % 2 == 0) {
                totalEvenCount++
            }
        }
        return totalEvenCount + (arrof.size)
    }
}

fun main() {
    DoubleEvenNumbers().solution()
}
