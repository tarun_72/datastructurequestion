package inteviewcamp.arrayandstring1

import DataStructureQuestion
import INTERVIEW_CAMP

class ReverseString : DataStructureQuestion() {
    override fun initInformation(): Information = Information(
        questionCount = 1.2,
        problemUrl = "",
        solutionUrl = "",
        source = INTERVIEW_CAMP
    )

    override fun solution() {
        var string: String = "i live in a house"
        reverse(string)

    }

    fun reverse(input: String): String {
        var chars = input.toCharArray()
        for (i in chars.size-1 downTo  0) {
            print(chars[i])

        }
        return ""
    }
}


fun main() {
    ReverseString().solution()
}
