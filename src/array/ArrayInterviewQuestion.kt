package array

import stringsleetcode.LeetCodeEasyString

class ArrayInterviewQuestion {
    //Array
    //Reverse the array
    //Anagram of array
    //Find the duplicates in array

    fun reverseTheArray(array: Array<Int>) {
        for (i in 0..array.size - 1) {
            println("before " + array[i])
        }
        var j = array.size - 1
        for (i in 0..array.size - 1) {
            if (i < j) {
                var swap: Int = array[i]
                array[i] = array[j]
                array[j] = swap
                j--
            }
        }
        for (i in 0..array.size - 1) {
            println("after  " + array[i])
        }
    }

    fun anagram(array: Array<Int>, anagramArray: Array<Int>): Boolean {
        if (array.size != anagramArray.size - 1) {
            return false
        }
        QuickSort().quickSort(array, 0, array.size - 1)
        QuickSort().quickSort(anagramArray, 0, anagramArray.size - 1)
        for (i in 0..array.size - 1) {
            if (array[i] != anagramArray[i]) {
                return false
            }
        }
        return true
    }
}

fun removeDuplicateFromArrayExtraaSpace(array: Array<Int>) {
    var temp: Array<Int> = Array(array.size, {
        0
    })
    var lastItem = array.get(0)
    temp[0] = lastItem
    var j = 1;
    for (i in 1..array.size - 1) {
        if (array[i] != lastItem) {
            temp[j] = array[i]
            j++
        } else {
            // do nothing
        }
        lastItem = array.get(i)
    }

    for (k in temp) {
        print("${temp[k]} ")
    }
}

fun removeDuplicatesWithoutExtraSpace(array: Array<Int>) {
    var lastItem = array.get(0);
    var counter = 1

    for (i in 1..array.size - 1) {
        if (array.get(i) == lastItem) {
            // oops do nothing
        } else {
            lastItem = array[i]
            array[counter] = array[i]
            counter++
        }
    }

}


fun main(arg: Array<String>) {
    var array = arrayOf(12, 54, 1, 65, 13, 90, 53)
    var arrayAnagram = arrayOf(12, 54, 1, 65, 13, 90, 53)
    var arrayAnagramNot = arrayOf(12, 54, 1, 65, 13, 90, 53)
    var arrayDuplicate = arrayOf(0, 0, 0, 1, 2, 2, 3, 4, 5, 5, 5, 6, 7, 7)
//    var arrayStock: Array<Int> = arrayOf(7, 1, 5, 3, 6, 4)
    var arrayStock: Array<Int> = arrayOf(1, 2, 3, 4, 5, 6, 7)
//    var arrayStock: Array<Int> = arrayOf(6,1,3,2,4,7)
//    var arrayStock: Array<Int> = arrayOf(2,1,2,0,1)
    var nums1 = arrayOf(1, 2, 2, 1)
    var nums2 = arrayOf(2, 2)
    var array1 = arrayOf(2, 9)
    var array2 = arrayOf(9, 9, 9)
    var array3 = arrayOf(9)
    var array4 = arrayOf(1, 9, 9)
    var arrayForZero = arrayOf(0, 1, 0, 3, 12)
    var arrayForSum = arrayOf(3, 2, 4)
    // BuySellStock().printMatrix(3)


//    print(BuySellStock().twoSum(arrayForSum.toIntArray(),6).toString())

    //ArrayInterviewQuestion().reverseTheArray(arrayOf(12, 54, 1, 65, 13, 90, 53))

    // QuickSort().quickSort(array, 0, 6)
//    removeDuplicateFromArrayExtraaSpace(arrayDuplicate)
    /*removeDuplicatesWithoutExtraSpace(arrayDuplicate)
      for (i in 0..arrayDuplicate.size - 1) {
          print(arrayDuplicate[i])
      }*/

    //   print(" prices ${BuySellStock().plusOne(array4.toIntArray()).forEach { print(it) }}")
//    print(LeetCodeEasyString().reverse(-2147483648))

    var testString = "0P"// "A man, a plan, a canal: Panama"
//    print(LeetCodeEasyString().isPalindrome(testString))
//    print(LeetCodeEasyString().myAtoi( "20000000000000000000"))

    //  print(LeetCodeEasyString().strStr("mississippi","issipi"))
    // print(LeetCodeEasyString().strStr("hello","ll"))
//    print(LeetCodeEasyString().countAndSay(8))
    // 31131211131221
  //  var string: Array<String> = arrayOf("flower", "flow", "flight")
    var string: Array<String> = arrayOf("c", "c")

    print(LeetCodeEasyString().longestCommonPrefix(string))

}