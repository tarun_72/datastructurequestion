package array

import javax.management.ListenerNotFoundException
import javax.print.attribute.IntegerSyntax

class BuySellStock {
    fun maxProfit(prices: IntArray): Int { // complexity n square lets reduce it
        var maximumPrize = 0
        for (i in 0..prices.size - 1) {
            for (j in i..prices.size - 1) {
                var difference = prices[j] - prices[i]
                if (maximumPrize < difference) {
                    maximumPrize = difference
                }
            }
        }
        return maximumPrize
    }


    fun maxProfitlessComplexity(prices: IntArray): Int { // complexity n square lets reduce it
        var maximumPrize = 0
        var minPrize = Int.MAX_VALUE


        for (counter in 1..prices.size - 1) {
            if (prices[counter] < minPrize) {
                minPrize = prices[counter]
            } else {
                var difference = prices[counter] - minPrize
                if (maximumPrize < difference) {
                    maximumPrize = difference
                }
            }
        }

        return maximumPrize
    }

    fun findArrayContainsDuplicates(nums: IntArray): Boolean {
        var map: HashMap<Int, Int> = HashMap()
        for (i in nums) {
            if (map.containsKey(i)) {
                return true
            } else {
                map.put(i, 1)
            }
        }
        return false
    }

    fun maxProfitlessComplexityMultiple(prices: IntArray): Int { // complexity n square lets reduce it
        var total: Int = 0
        for (counter in 1..prices.size - 1) {
            var profit = prices[counter] - prices[counter - 1]
            if (profit > 0) { // good to sold
                total += profit
            }
        }
        return total
    }

    fun rotate(nums: IntArray, k: Int): Unit {
        var n = nums.size
        // divide the two array
        reverseArrary(nums, 0, n.minus(k))
        reverseArrary(nums, n.minus(k).plus(1), n - 1)
    }

    fun reverseArrary(nums: IntArray, start: Int, End: Int): IntArray {
        var low = start
        var end = End
        while (low < end) {
            var temp = nums[low]
            nums[low] = nums[end]
            nums[end] = temp
            low++
            end--
        }
        //reverse them
        // than swap them
        return nums
    }


    fun singleNumber(nums: IntArray): Int {
        //2∗(a+b+c)−(a+a+b+b+c)=c
        var map: HashMap<Int, Int> = HashMap()
        nums.forEach {
            if (map.containsKey(it)) {
                var value = map.get(it)
                map.put(it, value?.plus(1) ?: 0)
            } else {
                map.put(it, 1)
            }
        }

        map.forEach {
            if (it.value == 1) {
                return it.key
            }
        }
        return 0
    }


    fun intersection1(nums1: IntArray, nums2: IntArray): IntArray {
        var set: HashSet<Int> = HashSet() // it will remove all the extraa thing
        nums1.forEach {
            set.add(it)
        }
        // find common in array two and set
        var result: ArrayList<Int> = ArrayList()
        nums2.forEach {
            if (set.contains(it)) {
                // we have found the similar element
                result.add(it)
                // we need to remove element from the stack
                // so that we will not be able to add element again
                set.remove(it)
            }
        }
        var resutArray: Array<Int> = Array(result.size, { 0 })
        for (i in 0..result.size - 1) {
            resutArray[i] = result.get(i)
        }
        return resutArray.toIntArray()
    }


    fun intersect(nums1: IntArray, nums2: IntArray): IntArray {
        val results = mutableListOf<Int>()
        val hashMap = HashMap<Int, Int>()

        for (value in nums1) {
            hashMap.get(value)?.let {
                hashMap.put(value, it + 1)
            } ?: run {
                hashMap.put(value, 1)
            }
        }

        for (value in nums2) {
            hashMap.get(value)?.let {
                results.add(value)
                if (it > 1)
                    hashMap.put(value, it - 1)
                else
                    hashMap.remove(value)
            }

        }
        return results.toIntArray()
    }

    fun plusOne(digits: IntArray): IntArray {
        val size = digits.size
        var increaseSize: Boolean = false
        var lastDigit = digits[size - 1]
        if (lastDigit != 9) {
            digits[size - 1] = digits.get(size - 1) + 1
        } else {
            var counter = 2
            while (lastDigit == 9 && counter <= size) {
                val secondLastDigit = digits[size - counter] // 2
                if (secondLastDigit != 9) {
                    digits[size - counter] = secondLastDigit.plus(1)
                    digits[size - counter.minus(1)] = 0
                    counter++
                } else {
                    lastDigit = secondLastDigit
                    if (size - counter == 0) {
                        // need to increase size of array
                        increaseSize = true
                    }
                    counter++
                }

            }
        }
        if (increaseSize || digits[0] == 9) {
            var array: Array<Int> = Array(size + 1, { 0 })
            array[0] = 1
            return array.toIntArray()
        }

        return digits
    }

    fun moveZeroes(nums: IntArray): Unit {
        var pointerToExchange = 0
        for (i in 0..nums.size - 1) {// 0, 1, 0, 3, 12 // 1,0,0,3,12
            if (nums[i] != 0) {
                nums[pointerToExchange] = nums[i]
                pointerToExchange++
            }
        }
        for (j in pointerToExchange..nums.size - 1) {
            nums[j] = 0
        }


        for (i in 0..nums.size - 1) {
            print("" + nums[i])
        }
    }

    fun twoSum(nums: IntArray, target: Int): IntArray {
        var map: HashMap<Int, Int> = HashMap()
        var list: ArrayList<Int> = ArrayList()

        // 3,
        for (i in 0..nums.size - 1) {
            val targetValue = target - nums[i]
            if (map.containsKey(targetValue)) {
                // we got it
                val k = map.get(targetValue) ?: 0
                list.add(k)
                list.add(i)

            } else {
                map.put(nums[i], i)
            }
        }
        return list.toIntArray()
    }

    fun printMatrix(n: Int) {
        for (i in 0..n - 1) {
            for (j in 0..n - 1) {
                print(" {$i$j}")
            }
        }
        println(" ")
        println("reverse order")
        println(" ")


        for (i in 0..n - 1) {
            for (j in (n - 1) downTo 0) {
                print(" {$j$i}")

            }
        }
    }


    fun rotate(matrix: Array<IntArray>): Unit {
        var list = listOf<Int>(1,3,6,7,3)
        for (i in 0..matrix.size - 1) {
            for (j in 0..matrix.size - 1) {
                // transpose
                val temp = matrix[i][j]
                matrix[i][j] = matrix[j][i]
                matrix[j][i] = temp
            }
        }

        for (i in 0..matrix.size - 1) {
            for (j in i..(matrix.size/2)) {
                val temp = matrix[i][j]
                matrix[i][j] = matrix[i][matrix.size.minus(1).minus(j)]
                matrix[i][matrix.size.minus(1).minus(j)] =  temp
            }
        }


    }
}



