package array

import ELEMENT_OF_PROGRAMMING


class DutchFlagProblem(override var concept: String) : ArrayBaseQuestions() {
    override fun initInformation(): Information = Information(
        questionCount = 6.1,
        problemUrl = "https://leetcode.com/problems/sort-colors/",
        solutionUrl = "https://log2base2.com/courses/algorithms/partition-function-implementation-quick-sort",
        source = ELEMENT_OF_PROGRAMMING
    )

    override fun solution() {
        var array: Array<Int> = arrayOf(2, 0, 2, 1, 1, 0)
        var p =  partition(array,0,array.size-1)

    }


    fun partition(array: Array<Int>, start: Int, end: Int): Int {
        var pivot:Int =  array[end]
        var pIndex:Int = 0
        for (begin in start until array.size){
            if(array[begin]<= pivot){
                swap(array,begin,pIndex)
                pIndex++
            }
        }
        swap(array, pIndex,pivot)
        return pIndex;
    }

    private fun swap(array: Array<Int>, first: Int, second: Int) {
        var temp = array[first]
        array[first] =  array[second]
        array[second] =  temp
    }
}