package array

class QuickSort {


    fun quickSort(array: Array<Int>, first: Int, last: Int) {
        if (last > first) {
            var partition = partition(array, first, last)
            quickSort(array, first, partition-1)
            quickSort(array, first = partition + 1, last = last)
        }
    }

    private fun partition(array: Array<Int>, first: Int, last: Int): Int {
        var pivotElement = array[first]
        var start = first
        var pivot = start
        var end = last

        while(start<end){
            while(array[start]<= pivotElement && start< end){
                start++
            }
            while(array[end]>pivotElement){
                end--
            }
            if(start< end){
            var swap = array[start]
            array[start] = array[end]
            array[end] = swap
                start++
                end--
        }

        }

        //swap pivot element
        var swap = array[first]
        array[first] = array[end]
        array[end] = swap
        return  end
    }
}