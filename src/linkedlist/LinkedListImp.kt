package linkedlist

// if we need traverse list use currentNode.next != null
// if u want to remove last item of list use currentNode != null
class LinkedListImp<T : Comparable<T>>(override var count: Int) : List<T> {
    var rootNode: Node<T>? = null
    var size: Int = 0

    override fun insert(dataInfo: T) {
        size++
        if (rootNode == null) {
            // add root node
            rootNode = Node<T>(dataInfo)
            rootNode?.nextNode = null
        } else {
            // root node is not null
            insertAtEnd(dataInfo)
        }
    }

    private fun insertItemAtBeginning(dataInfo: T) {// insert 5
        if (rootNode != null) { // 13 54 12
            var newNode = Node<T>(dataInfo) //
            newNode?.nextNode = rootNode
            this.rootNode = newNode
        }

    }


    private fun insertAtEnd(dataInfo: T) {
        if (rootNode != null) {
            var currentNode: Node<T>? = rootNode
            while (currentNode?.nextNode != null) { // it will go for last node
                currentNode = currentNode.nextNode
            }
            var newNode = Node<T>(dataInfo) //
            newNode?.nextNode = null
            currentNode?.nextNode = newNode
        }           

    }

    override fun remove(dataInfo: T) {
        if (rootNode != null) {
            if (rootNode?.dataInfo == dataInfo) {
                // rootNode  need to remove
                var node: Node<T>? = rootNode?.nextNode
                rootNode = node
                size--
            } else {
                var currentNode: Node<T>? = rootNode
                while (currentNode != null) {
                    if (currentNode.dataInfo == dataInfo) {
                        // remove node
                        var node: Node<T>? = currentNode?.nextNode
                        currentNode = node
                        size--
                    }
                    currentNode = currentNode?.nextNode
                }
            }
        }
    }

    override fun size(): Int {
        return size
    }

    override fun traverseList() {
        if (rootNode != null) {
            var currentNode: Node<T>? = rootNode
            while (currentNode != null) {
                println("${currentNode.dataInfo}")
                currentNode = currentNode.nextNode
            }
        }
    }

    fun findtheMiddleNode(): Node<T>? {
        if (rootNode == null) {
            return null
        } else {
            var nodeCounter = 0
            var slowNode: Node<T>? = rootNode
            var currentNode: Node<T>? = rootNode
            while (currentNode?.nextNode != null) {
                nodeCounter++
                slowNode = slowNode?.nextNode
                if (currentNode.nextNode != null) {
                    currentNode = currentNode?.nextNode?.nextNode
                }

            }
            return slowNode

        }
    }

    // 1->2->3->4->5
    // temp = 0 , current node = 1 nextNode = 2
    fun reverseLinkedList() {
        if (rootNode == null) {
            // no need to anything
        } else {
            var tempNode: Node<T>? = null
            var currentNode: Node<T>? = rootNode
            while (currentNode != null) {
                var NextNode: Node<T>? = currentNode.nextNode
                currentNode.nextNode = tempNode
                tempNode = currentNode
                currentNode = NextNode
            }
            rootNode = tempNode
        }
    }

}