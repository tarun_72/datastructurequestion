package linkedlist

fun main(arg: Array<String>) {
    var list: LinkedListImp<Int> = LinkedListImp(2)
    list.insert(1)
    list.insert(2)
    list.insert(3)
    list.insert(4)
    list.insert(5)
    // list.traverseList()
    println("${list.size}")
    // list.remove(1)
    // list.traverseList()
    println("${list.size}")
    // list.remove(5)
    // list.traverseList()
    println("${list.size}")

    // println(list.findtheMiddleNode())
    list.reverseLinkedList()
    list.traverseList()
}
