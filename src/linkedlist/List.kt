package linkedlist

interface List<T> {
     var count:Int
    fun insert(dataInfo: T)
    fun remove(dataInfo: T)
    fun size(): Int
    fun traverseList()

}