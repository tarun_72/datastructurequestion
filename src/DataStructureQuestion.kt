const val ELEMENT_OF_PROGRAMMING = "ElementsOfProgramming"
const val  CRACKING_CODING_INTERVIEW = "CrackingCodingInterview"
const val  POPULAR_CODING_INTERVIEW_PROBLEMS_50 = "50 popular coding interview problems"
const val  LEET_CODE_ARRAY = "LEET CODE ARRAY"
const val  INTERVIEW_CAMP = "Interview Camp "




 abstract class DataStructureQuestion {
    lateinit var information: Information
    abstract fun initInformation(): Information;
    abstract fun solution()
    fun information() {
        information = initInformation()
        print(information)
        solution()

    }

    data class Information(
        var questionCount: Double,
        var problemUrl: String,
        var solutionUrl: String,
        var source: String
    )
}
