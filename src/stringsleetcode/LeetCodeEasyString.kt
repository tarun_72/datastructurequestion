package stringsleetcode

import java.lang.StringBuilder

class LeetCodeEasyString {
    /*Input: ["h","e","l","l","o"]
     *Output: ["o","l","l","e","h"]
    */
    fun reverseString(s: CharArray): Unit {
        var end = s.size - 1
        var start = 0
        while (start < end) {
            val temp = s[start]
            s[start] = s[end]
            s[end] = temp
            start++
            end--
        }
    }


    fun reverse(x: Int): Int {
        var sum: Long = 0
        var digit = x

        while (digit != 0) {
            val remainder = digit % 10
            val question = digit / 10
            sum = sum * 10 + remainder
            digit = question
        }
        if (sum > 2147483648) {
            return 0
        } else if (sum < -2147483648) {
            return 0

        }
        return sum.toInt()
    }

    fun isAnagram(s: String, t: String): Boolean {
        // sort both arrays
        // check their index
        if (s.length != t.length) {
            return false
        } else {
            // length is equal
            var sarray = s.toCharArray()
            var tarray = t.toCharArray()
            sarray.sort()
            tarray.sort()
            for (i in 0..sarray.size - 1) {
                if (sarray[i] != tarray[i]) {
                    return false
                }
            }
        }
        return true
    }

    fun isPalindrome(s: String): Boolean {
        // first remove all the other character except alphanumeric
        var stringBuffer: StringBuffer = StringBuffer()
        for (i in 0..s.length - 1) {
            val c: Int = s[i].toInt()
            if (c in 48..57 || c in 65..90 || c in 97..122) {
                stringBuffer.append(s[i])
            }
        }
        var alphaNumericString = stringBuffer.toString()
        alphaNumericString = alphaNumericString.toLowerCase()
        var start = 0
        var end = alphaNumericString.length - 1
        while (start < end) {
            if (alphaNumericString[start] == alphaNumericString[end]) {
                // good
            } else {
                return false
            }

            start++
            end--

        }
        return true
    }

    fun myAtoi(str: String): Int {
        val plusSign = 43
        val negative = 45
        val strr = str.trim()
        var list: String = ""
        var signner = 1
        if (strr.length > 0) {
            val firstChar = strr[0].toInt()
            if (firstChar == plusSign ||
                firstChar == negative ||
                (firstChar in 48..57)
            ) {
                var counter = 0
                var previousIsSign = false
                while (counter < strr.length) {
                    var nextInt = strr[counter].toInt()
                    if (nextInt == plusSign ||
                        nextInt == negative ||
                        (nextInt in 48..57)
                    ) {
                        if (nextInt == plusSign) {
                            signner = 1
                            if (!previousIsSign) {
                                previousIsSign = true
                            } else {
                                break
                            }

                        } else if (nextInt == negative) {
                            signner = -1

                            if (!previousIsSign) {
                                previousIsSign = true
                            } else {
                                break
                            }

                        } else {
                            list = list.plus(strr[counter])
                        }
                    } else {
                        break
                    }
                    counter++
                }
                if (list.length > 0) {
                    val value: Long = list.toLong() * signner
                    if (value >= Integer.MAX_VALUE) {
                        return Integer.MAX_VALUE
                    } else if (value <= Integer.MIN_VALUE) {
                        return Integer.MIN_VALUE
                    }
                    return list.toInt() * signner
                }
            } else {
                return 0
            }
        }
        return 0
    }

    fun strStr(haystack: String?, needle: String?): Int {
        val String_Not_Found = -1
        val String_Empty = 0
        if (haystack != null && needle != null) {
            if (needle.isEmpty()) return String_Empty
            if (haystack.isEmpty()) return String_Not_Found
            if (needle.length > haystack.length) return String_Not_Found
            var i = 0
            while (i < haystack.length) {
                var needleCounter = 0
                for (c in i..(i + (needle.length - 1))) {
                    if (needleCounter > needle.length - 1) {
                        break
                    }
                    if (needleCounter < needle.length &&
                        c < haystack.length &&
                        haystack[c] == needle[needleCounter]
                    ) {
                        needleCounter++

                        if (c == (i + needle.length - 1)) {
                            // we have found the result
                            return i
                        }
                    } else {
                        needleCounter = 0
                        break
                    }
                }
                i++
            }
        }
        return String_Not_Found
    }

    fun countAndSay(n: Int): String {

        val firstElement: String = "1"
        var stringBuilder: StringBuilder = StringBuilder()
        stringBuilder.append(firstElement) // level 1
        var result: String = stringBuilder.toString() // result for n = 1
        if (n == 0) { // boundary case
            return ""
        } else {
            var i = 1
            while (i < n) {
                var temp = stringBuilder.toString() // 1
                var count = 1
                var previousChar = temp[0] // 1
                stringBuilder.clear()
                for (j in 1..temp.length - 1) { // 1
                    //  111221
                    // 21
                    var charFound = temp[j]
                    if (charFound == previousChar) {
                        count++
                    } else {
                        stringBuilder.append(count)
                        stringBuilder.append(previousChar)
                        previousChar = charFound
                        count = 1
                    }

                }
                stringBuilder.append(count)
                stringBuilder.append(previousChar)
                i++
            }

        }
        result = stringBuilder.toString()
        return result;

    }

    fun longestCommonPrefix(strs: Array<String>): String {
        return ""
    }
}
