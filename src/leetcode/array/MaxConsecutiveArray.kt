package leetcode.array

import DataStructureQuestion
import LEET_CODE_ARRAY
import POPULAR_CODING_INTERVIEW_PROBLEMS_50
import interviewquestionudemycourse1.TwoSum

class MaxConsecutiveArray : DataStructureQuestion() {
    override fun initInformation(): Information = Information(
        questionCount = 1.1,
        problemUrl = "https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/",
        solutionUrl = "",
        source = LEET_CODE_ARRAY
    )

    override fun solution() {

        var arrof: IntArray = IntArray(7)
        arrof[0] = 1
        arrof[1] = 1
        arrof[2] = 0
        arrof[3] = 1
        arrof[4] = 1
        arrof[5] = 1
        arrof[6] = 1


        var result = findMaxConsecutiveOnes(arrof)
        println("result $result")
    }


    fun findMaxConsecutiveOnes(nums: IntArray): Int {
        var max: Int = 0
        var current: Int = 0
        for (i in nums.indices) {
            if (nums[i] == 1) {
                ++current
                if (current > max) {
                    max = current
                }
            }else{
                current = 0
            }
        }
        return max
    }
}

fun main() {

    MaxConsecutiveArray().solution()
}