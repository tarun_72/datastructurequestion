package leetcode.array

import DataStructureQuestion
import LEET_CODE_ARRAY

class SquaresSortedArray : DataStructureQuestion() {
    override fun initInformation(): Information = Information(
        questionCount = 1.1,
        problemUrl = "https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/",
        solutionUrl = "",
        source = LEET_CODE_ARRAY
    )

    override fun solution() {

        var arrof: IntArray = IntArray(5)
        arrof[0] = -4
        arrof[1] = -1
        arrof[2] = 0
        arrof[3] = 3
        arrof[4] = 10
        var values = sortedSquares(arrof)
        values.forEach {
            println(it)
        }
    }

    fun sortedSquares(nums: IntArray): IntArray {
        for (i in nums.indices) {
            var value = getSquared(nums[i])
            nums[i] = value
        }
        quickSort(nums,0,nums.size-1)
        return nums
    }

    fun getSquared(number: Int) = number * number

    fun quickSort(nums: IntArray, start: Int, end: Int) {
        if (start < end) {
            var partition: Int = partition(nums, start, end)
            quickSort(nums, start, partition - 1)
            quickSort(nums, partition + 1, end)
        }
    }

    fun partition(array: IntArray, start: Int, end: Int): Int {
        var pivot:Int =  array[end]
        var pIndex:Int = 0
        for (begin in array.indices){
            if(array[begin]<= pivot){
                swap(array,begin,pIndex)
                pIndex++
            }
        }
        swap(array, pIndex,pivot)
        return pIndex;
    }

    private fun swap(array: IntArray, first: Int, second: Int) {
        var temp = array[first]
        array[first] =  array[second]
        array[second] =  temp
    }
}


fun main() {

    SquaresSortedArray().solution()
}