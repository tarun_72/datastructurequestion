package leetcode.array

import DataStructureQuestion
import LEET_CODE_ARRAY

class EvenNumberOfDigits : DataStructureQuestion() {
    override fun initInformation(): Information = Information(
        questionCount = 1.1,
        problemUrl = "https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/",
        solutionUrl = "",
        source = LEET_CODE_ARRAY
    )

    override fun solution() {

        var arrof: IntArray = IntArray(5)
        arrof[0] = 12
        arrof[1] = 345
        arrof[2] = 2
        arrof[3] = 6
        arrof[4] = 7896

        var arrof2: IntArray = IntArray(4)
        arrof2[0] = 555
        arrof2[1] = 901
        arrof2[2] = 482
        arrof2[3] = 1771

        var arrof3: IntArray = IntArray(1)
        arrof3[0] = 100000

        val result = findNumbers(arrof)
        println("result $result")
    }


    fun findNumbers(nums: IntArray): Int {
        var evendigits: Int = 0
        for (i in nums.indices) {
            val number = nums[i]
            var digits: Int = getDigitCount(number)
            if (digits % 2 == 0) {
                evendigits++
            }
        }
        return evendigits
    }

    fun getDigitCount(number: Int): Int {
        var digit = number
        var count = 0
        while (digit > 0) {
            count++
            val num = digit / 10
            digit = num
        }
        return count
    }
}


fun main() {

    EvenNumberOfDigits().solution()
}