package interviewquestionudemycourse1

import DataStructureQuestion
import POPULAR_CODING_INTERVIEW_PROBLEMS_50

class TwoSum : DataStructureQuestion() {
    override fun initInformation(): Information = Information(
        questionCount = 1.1,
        problemUrl = "https://leetcode.com/problems/two-sum/",
        solutionUrl = "",
        source = POPULAR_CODING_INTERVIEW_PROBLEMS_50
    )

    override fun solution() {
        var arrof: IntArray = IntArray(4)
        arrof[0] = 2
        arrof[1] = 7
        arrof[2] = 11
        arrof[3] = 15


        var result = twoSum(arrof, 9)
        result.forEach {
            println(it)
        }
    }

    /**
     * 1. Brute force n2
     * 2.  use Left and right pointer if array is sorted nlogn
     * 3. use hashmaps
     * */
    fun twoSum(nums: IntArray, target: Int): IntArray {
        // value  // Index
        var map: HashMap<Int, Int> = HashMap()
        for (i in nums.indices) {
            if (map.containsKey(nums[i])) {
                //arrayOf(nums[i],nums[map.get(nums[i])?:0])
                var arra: IntArray = IntArray(2)
                arra[1] = i
                arra[ 0]= map.get(nums[i])?:0
                return arra
            } else {
                map.put((target - nums[i]), i)
            }
        }
        return IntArray(2)
    }
}

fun main() {

    TwoSum().solution()
}