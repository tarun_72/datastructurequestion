package crackingcodingInterview

import CRACKING_CODING_INTERVIEW
import DataStructureQuestion

// Implement an algorithm to determine
// if a string has all unique character what if you cannot use addition data structures
class ArrayStringFirst : DataStructureQuestion() {
    override fun initInformation(): Information = Information(
        questionCount = 1.1,
        problemUrl = "https://leetcode.com/problems/contains-duplicate/",
        solutionUrl = "",
        source = CRACKING_CODING_INTERVIEW
    )

    override fun solution() {
        var arrof: Array<Int> = arrayOf(1, 2, 3, 1)
        containsDuplicate(arrof)
    }

    fun containsDuplicate(nums: Array<Int>): Boolean {
        // 1. hash map strategies, where we put items in hashmap and go
        // add 2 for loops  to check whether it is contains duplicates
        // sort the array and than find the duplicates
        // if do not have any other data structure
        var hashMap: HashMap<Int,Boolean> =  HashMap()

        for(i in nums){
            if(hashMap.containsKey(i)){
                return true
            }else{
                hashMap[i] = true
            }
        }
        return false
    }
}